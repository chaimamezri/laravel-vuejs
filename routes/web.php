<?php

use Illuminate\Support\Facades\Route;


//Route::name('root_path')->get('/','App\Http\Controllers\PagesController@home');

Route:: get('/test-email', function() {
   return new App\Mail\ContactMessageCreated('Chaima Mezri' , 'mezrichaima@gmail.com' , 'Merci pour Lacarte.') ;
}
);

Route:: get('/', [
    'as' => 'root_path',
    'uses' => 'App\Http\Controllers\PagesController@home'
]);

//Route::get('/about','App\Http\Controllers\PagesController@about')->name('about_path');
Route:: get('/about', [

    'as' => 'about_path',
   'uses' => 'App\Http\Controllers\PagesController@about'
]);

Route:: get('/contact', [

    'as' => 'contact_path',
    'uses' => 'App\Http\Controllers\ContactsController@create'
]);

Route:: post('/post-contact', [

    'as' => 'post_contact_path',
    'uses' => 'App\Http\Controllers\ContactsController@store'
]);









@extends('layouts.default', ['title' => 'About'])

@section('content')
    <div class="container">

        <h2>What is {{config('app.name')}}?</h2>
        <p>{{config('app.name')}} is a clone app  of <a href="https://larampa.com" target="_blank">Laramap.com </a></p> {{-- target thez l autre page--}}

        <div class="row">
            <div class="col-md-6">
                <p class="alert alert-warning">
                <strong> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This app
                    has been built by <a href="https://twitter.com/etsmo">@etsmo </a>for learning purposes.</strong>
                </p>

            </div>
        </div>

        <p>Feel free to help to improve the <a href="https://gitlab.com/chaimamezri/laracarte.git">source code</a>.</p>

        <hr>

        <h2>What is Laramap?</h2>
        <p>Laramp is the website by which {{config('app.name')}} was inspired :) . </p>

        <p>More info<a href="https://laramap.com/p/about" > here</a>.</p>

        <hr>
    <h2>Which tools and services are used in {{config('app.name')}}?</h2>
        <p>Basically it's a built on laravel &amp;Bootsrap.But there's of services used for email and other sections. </p>
  <ul>

      <li>Laravel</li>
      <li>Bootsrap</li>
      <li>Amazon S3</li>
      <li>Google Maps</li>
      <li>Gravatar</li>
      <li>Heroku</li>
  </ul>

    </div>


@stop

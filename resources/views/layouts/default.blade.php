<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>{{ page_title($title ?? '') }}</title>    <!--  ?? php 7 si existe hot $title sinon hot '' ken heki sinon manhothech ken manhotlouch title me fou9 tjini error  et pages_title function helpers-->


    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- fontawesome -->
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">

    <!-- Latest Compiled and minified CSS-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    <style>

        body{
            font-family: 'open Sans', Helvetica, Arial, sans-serif;
        }

        footer{
           /* margin-top: 4em;
            margin-bottom: 4em;*/
            margin: 4em 0;
        }
    </style>


</head>
<body>
@include('layouts/partials/_nav')


@yield('content')

@include('layouts/partials/_footer')



<script src="//code.jquery.com/jquery.min.js"> </script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>


@extends('layouts.default', ['title' => 'Contact'])

@section('content')
    <div class="container">

{{--      pour décaler lil west --}}
        <div class="row">
            {{-- teba3 bootsrap heda 12 colonnes andou bootsrap 8colonnes w n5ali 2 al limin w 2 allisar // w sm heki ya3ni lilmobile 10 fl west w 1 allimin w wahda al lisa  --}}
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

                <h2>Get In Touch</h2>
                <p class="text-muted">If you having trouble with this service, please <a href="
                mailto:{{ config('laracarte.admin_support_email') }}"> ask for help</a>.</p>

                <form action="{{ route('post_contact_path') }}" method="POST" novalidate >  {{-- *******novalidate nzidha ba3ed l POST" bech kinenzel ala champ te3 Name ou Email may3mlich voueillez renseigner ce champ  ou chouf louta ****** button  --}}
                    {{--ki tjini page expiré lezemha refresh  donc lezem csrf_field()--}}
                   {{ csrf_field() }}

                    <div class="form-group {{$errors->has('name')? 'has-error' : ''}} ">
                        <label for="name" class="control-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required="required"
                               value="{{ old('name') }}">
                        {!!$errors->first('name','<span class="help-block">:message</span>')!!}
                  </div>

                  <div class="form-group {{$errors->has('email')? 'has-error' : ''}} ">
                      <label for="email" class="control-label">Email</label>
                      <input type="email" name="email" id="email" class="form-control" required="required"
                             value="{{ old('email') }}">
                      {!!$errors->first('email','<span class="help-block">:message</span>')!!}
                    </div>

                    <div class="form-group {{$errors->has('message')? 'has-error' : ''}} ">
                        {{-- sr-only ahyka behc matbaynlkch Message flafichage kima Name w Email --}}
                        <label for="message" class="control-label sr-only" >Message</label>
                        <textarea class="form-control" rows="10" cols="10" required="required" name="message" id="message" >{{ old('message') }} </textarea>
                        {!!$errors->first('message','<span class="help-block">:message</span>')!!}
                    </div>

                    <div class="form-group">
                        {{-- btn-block largeur
                         et &raquo; lil fleshe hehda  >>
                          btn btn-default  blanc loun
                          btn btn-primary bleu --}}
{{--     {{--  ********<button class=" btn btn-primary btn-block" formnovalidate>Submit Enquiry &raquo;</button>--}}
                        <button class=" btn btn-primary btn-block" type="submit">Submit Enquiry &raquo;</button>

                    </div>

                </form>
            </div>
        </div>



    </div>


@stop

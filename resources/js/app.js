import Vue from "vue";

require('./bootstrap');

window.Vue = require('vue');

Vue.component('select-categories-component', require('./components/SelectCategoriesComponent.vue').default);
Vue.component('stats-table-component', require('./components/StatsTableComponent.vue').default);

const app = new Vue({
    el: '#app',
    components: {
        'select-categories-component': require('./components/SelectCategoriesComponent.vue').default,
        'stats-table-component': require('./components/StatsTableComponent.vue').default
    }
});

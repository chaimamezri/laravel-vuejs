<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessageCreated;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;  //alt Maj Entrééé
use Illuminate\Support\Facades\Mail;  //alt Maj Entrééé

class ContactsController extends Controller
{
    public function create(){

        return view('contacts.create');
    }

    public function store(ContactRequest $request){  //sera appeler quand on va soumettre notre formulaire t post

       //dd behc nwarik kifeh fazet clé valeur eli heya taht star eli taht hehda
        //dd($request->only('name','email' , 'message' )); // tab de clé valeur hné clé name  valeur $request->name comme ça
        // "name" => "Chaima Mezri"
        //  "email" => "mezrichaima@gmail.com"
        //  "message" => "Je suis un message ok ok ok"
        $message = Message::create($request->only('name','email' , 'message' ));


       // $mailable = new ContactMessageCreated($message); kenet $mailable f west send

        Mail::to(config('laracarte.admin_support_email'))
              ->send(new ContactMessageCreated($message));

        return 'Done!';






    }
}
